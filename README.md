# express-minisign

**[Currently unsupported in Node 12](https://github.com/sodium-friends/sodium-native/issues/95)**

A Minisign based middlware for express.

Based on [Minisign](https://jedisct1.github.io/minisign/) and it's [JavaScript API](https://github.com/chm-diederichs/minisign), this middleware allows you to serve up files for clients to download which include a header that can be verified against your Minisign public key.

## Usage

### Create a public/secret key pair
You'll need to create a Minisign instance on your server and generate a public/secret key pair.  The public key should be served on it's own route so that clients can verify future downloads with it, the password and private key should be kept private.

### Serve downloads that have been signed

The middleware takes options that include:

**minisignPassword**:  This is a plaintext password that is used when you generate your public and secret key pair.  It should be stored somewhere safely on your server.

**minisignSecretKeyFilePath**:  This is the path to the private key file.  this file should not be served publicly and should be stored somewhere your server has access to.

**serveFile/filePath**:

*serveFile* should be a function that returns an object containing a filePath.

*filePath* should be an object that has a filePath.

**Only one of either serveFile or filePath these should be included in the options object**

This will be the file that is signed and offered fow download to the client.

```[javascript]
const getFile = (fileName) => {
    //do something with the filename
    return { filePath: pathToFile}
};
const options = {
    minisignPassword: password,
    minisignSecretKeyFilePath: pathToSecretKey,
    serveFile: getFile,
};

app.use('/download', minisignExpress(options));
```

or

```[javascript]
const filePath = {filePath: pathToFile};
const options = {
    minisignPassword: password,
    minisignSecretKeyFilePath: pathToSecretKey,
    filePath: filePath,
};

app.use('/download', minisignExpress(options));
```

## Optional options

The following are optional options.

### sendFileOptions, default to `{}`

[The express res.sendFile() options](http://expressjs.com/en/4x/api.html#res.sendFile).  If not passed through, it will default to an empty object: `let obj = {};`

### options.signContentOptions, default to `{}`

[The minisign javascript options](https://github.com/chm-diederichs/minisign#signing-content-provided-as-buffer).  If not passed through, it will default to an empty object: `let obj = {};`

## Usage for clients

The client will be served the exact same file they expect, however they have the opportunity of being able to verify the file is from you by using Minisign to verify the file.

A header of `x-file-minisign-signature` is included in the response that contains the signature in `base64` format.  This can be then decoded using:

```[javascript]
const signatureBase64 = Buffer.from(res.header['x-file-minisign-signature'], 'base64');
const signatureAsString = signatureBase64.toString('ascii');
```

Where the decoded signature can then be passed to the clients implementation of Minisign, along with your public key and the file you served to be verified.
